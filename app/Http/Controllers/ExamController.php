<?php

namespace App\Http\Controllers;

use App\Grade;
use Illuminate\Http\Request;
use PHPExcel_IOFactory;
use App\SubjectCode;
use App\Question;
use App\Part;
use App\ExamHistory;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check() == false) {

            return redirect('login');
        }

        $subjects = SubjectCode::all();

        return view('listExamToDo', compact('subjects'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function importQuestionForm()
    {
        if (auth()->check() == false) {

            return redirect('login');
        } else {

            if (auth()->user()->role != 'admin') {

                return redirect('login');
            }
        }

        $list_subject_code = SubjectCode::all()->toArray();

        return view('importQuestion', compact('list_subject_code'));
    }

    public function importQuestionExcel(Request $request)
    {
        if (auth()->check() == false) {

            return redirect('login');
        } else {

            if (auth()->user()->role != 'admin') {

                return abort(403, "No permission");
            }
        }

        $request->validate([

            'import_file' => 'required',
            'subject_code' => 'required'

        ]);

        $subject_code = $request->input('subject_code');

        SubjectCode::emptySubject($subject_code);

        $part_id_list = Part::where('subject_code_id', '=', $subject_code)->orderBy('id')->get()->toArray();

        $path = $request->file('import_file')->getRealPath();

        $objPHPExcel = PHPExcel_IOFactory::load($path); // load file ra object PHPExcel

        $currentSheet = 0;

        while ($currentSheet < 7) {

            $provinceSheet = $objPHPExcel->setActiveSheetIndex($currentSheet); // Set sheet sẽ được đọc dữ liệu
            $highestRow    = $provinceSheet->getHighestRow(); // Lấy số row lớn nhất trong sheet
            $currentSheet++;
            for ($row = 2; $row <= $highestRow; $row++) { // For chạy từ 2 vì row 1 là title

                $wh = $provinceSheet->getCellByColumnAndRow(0, $row)->getValue();
                $correct_ans = $provinceSheet->getCellByColumnAndRow(1, $row)->getValue();
                $ans_1 = $provinceSheet->getCellByColumnAndRow(2, $row)->getValue();
                $ans_2 = $provinceSheet->getCellByColumnAndRow(3, $row)->getValue();
                $ans_3 = $provinceSheet->getCellByColumnAndRow(4, $row)->getValue();
                $ans_4 = $provinceSheet->getCellByColumnAndRow(5, $row)->getValue();
                $attachment = $provinceSheet->getCellByColumnAndRow(6, $row)->getValue();
                $part_id = 0;

                foreach ($part_id_list as $part) {

                    if ($part['part'] == $currentSheet) {

                        $part_id = $part['id'];
                    }
                }


                if (!empty($wh) && !empty($correct_ans) && !empty($ans_1) && !empty($ans_2) && !empty($ans_3)) {
                    $question_data = array(
                        'the_question' => $wh,
                        'correct_answer' => $correct_ans,
                        'part_id' => $part_id,
                        'answer_1' => $ans_1,
                        'answer_2' => $ans_2,
                        'answer_3' => $ans_3,
                        'answer_4' => $ans_4,
                        'attachment' => $attachment,
                    );

                    Question::saveQuestion($question_data);
                } else {

                    return \Redirect::back()->with('error', 'Data is not correct at sheet '.$currentSheet.' on row '.$row);
                }
            }


        }

        return \Redirect::back()->with('msg', 'Import questions Success');
    }

    public function submitExam(Request $request)
    {
        $listening_correct_count = $request->listening_correct_count;
        $reading_correct_count = $request->reading_correct_count;

        $listening_grade = $listening_correct_count != 0 ? Grade::find($listening_correct_count)->toArray()['grade_listening'] : 0;
        $grade_reading = $reading_correct_count != 0 ? Grade::find($reading_correct_count)->toArray()['grade_reading'] : 0;

        $exam_history = new ExamHistory();
        $exam_history->user_id = $request->user_id;
        $exam_history->subject_code_id = $request->subject_code_id;
        $exam_history->listening_correct = $listening_correct_count;
        $exam_history->reading_correct = $grade_reading;
        $exam_history->total_grade = $listening_grade + $grade_reading;
        $exam_history->save();

        $result['grade_listening'] = $listening_grade;
        $result['grade_reading'] = $grade_reading;
        $result['grade_total'] = $listening_grade + $grade_reading;

        echo json_encode($result);
    }

    public function doExam($id)
    {
        if (auth()->check() == false) {

            return redirect('login');
        }

        $subject = new SubjectCode();

        $random_exam = $subject->get_exam_by_id($id);

        if ($random_exam != false) {

            return view('doExam', compact('random_exam'));
        } else {

            return abort(403, "No permission");
        }
    }

    public function importCalculateGradeForm(Request $request)
    {
        Grade::truncate();

        $path = $request->file('import_file')->getRealPath();

        $objPHPExcel = PHPExcel_IOFactory::load($path); // load file ra object PHPExcel

        $provinceSheet = $objPHPExcel->setActiveSheetIndex(0); // Set sheet sẽ được đọc dữ liệu

        $highestRow    = $provinceSheet->getHighestRow(); // Lấy số row lớn nhất trong sheet

        for ($row = 2; $row <= $highestRow; $row++) { // For chạy từ 2 vì row 1 là title

            $total_correct = $provinceSheet->getCellByColumnAndRow(0, $row)->getValue();
            $grade_listening = $provinceSheet->getCellByColumnAndRow(1, $row)->getValue();
            $grade_reading = $provinceSheet->getCellByColumnAndRow(2, $row)->getValue();


            if ( is_numeric($total_correct) && is_numeric($grade_listening) && is_numeric($grade_reading)) {
                $grade_data = array(
                    'total_correct' => $total_correct,
                    'grade_listening' => $grade_listening,
                    'grade_reading' => $grade_reading,
                );

                Grade::create($grade_data);

            } else {
                return \Redirect::back()->with('error', 'Data is not correct on row '.$row);
            }

        }

        return \Redirect::back()->with('msg', 'Import Grades Success');
    }

    public function highScore()
    {
        $top_high_score = ExamHistory::take(10)->orderBy('total_grade', 'DESC')->get();
        return view('high_score', compact('top_high_score'));
    }
}
