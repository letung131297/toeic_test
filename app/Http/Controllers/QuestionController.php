<?php

namespace App\Http\Controllers;
use App\Question;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $listQuestion = Question::all();
        return view('listQuestion', compact('listQuestion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $question = Question::find($id);
        return view('editQuestion', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $question = Question::find($id);

        $request->validate([

            'the_question' => 'required',
            'correct_answer' => 'required',
            'answer_1' => 'required',
            'answer_2' => 'required',
            'answer_3' => 'required',
            'answer_4' => 'required',

        ]);

        $question->the_question = $request->input('the_question');
        $question->correct_answer = $request->input('correct_answer');
        $question->answer_1 = $request->input('answer_1');
        $question->answer_2 = $request->input('answer_2');
        $question->answer_3 = $request->input('answer_3');
        $question->answer_4 = $request->input('answer_4');

        if ($request->file()) {

            if(!empty($question->attachment)) {

                Storage::delete('public/subject' . $id.'/'. $question->attachment);

            }
            $path = $request->file('attachment')->store('public/subject'.$id.'/images_questions/');
            $question->attachment = Storage::url($path);
        }

        $question->save();

        return redirect()->route('questions.index')->with('msg', 'Edit Question Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeAttachment($id)
    {
        $question = Question::find($id);
        $question->attachment = '';
        $question->save();

        return redirect()->back()->with('msg', 'Remove attachment Success');
    }
}
