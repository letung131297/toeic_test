<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\SubjectCode;
use App\Part;

class SubjectCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $listSubject = SubjectCode::all();
        return view('listSubjectCode', compact('listSubject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $request->validate([

            'title' => 'required',

        ]);

        $subject = new SubjectCode();
        $subject->title = $request->input('title');
        $subject->description = $request->input('description');

        $subject->save();
        $id_subject = $subject->id;  // last inserted id

        //create 7 parts for subject
        for ($part = 1; $part <= 7; $part++) {
            $obj_part = new Part();
            $obj_part->part = $part;
            $obj_part->subject_code_id = $id_subject;
            $obj_part->save();
        }

        // include the listening file
        if ($request->file()) {
            if(!empty($subject->listening_file)) {

                Storage::delete('public/subject' . $id_subject.'/'. $subject->listening_file);

            }
            $path = $request->file('listening_file')->store('public/subject'.$id_subject);

            $listening_file = Storage::url($path);

            $subject = SubjectCode::find($id_subject);
            $subject->listening_file = $listening_file;
            $subject->save();

        }

        return \Redirect::back()->with('msg', 'Add Subject Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $subject = SubjectCode::find($id);
        return view('editSubjectCode', compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $request->validate([

            'title' => 'required',

        ]);

        $subject = SubjectCode::find($id);
        $subject->title = $request->input('title');
        $subject->description = $request->input('description');
        if ($request->file()) {
            if(!empty($subject->listening_file)) {

                Storage::delete('public/subject' . $id.'/'. $subject->listening_file);

            }
            $path = $request->file('listening_file')->store('public/subject'.$id);
            $subject->listening_file = Storage::url($path);
        }

        $subject->save();

        return redirect()->route("subjects.index")->with('msg', 'Edit Subject Success');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $subject = SubjectCode::find($id);
        Storage::deleteDirectory('public/subject' . $id);

        if($subject->deleteSubject($id)) {
            Storage::deleteDirectory('subject' . $id);

            return \Redirect::back()->with('msg', 'Delete Success');
        }

        return \Redirect::back()->withErrors('error', 'Delete fail');
    }
}
