<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $users = User::all();
        return view('listUser', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        if (empty($name) || empty($email) || empty($password)) {

            return redirect()->back()->with('error', 'Invalid fields');

        } else {

            $new_user = new User();
            $new_user->name = $name;
            $new_user->email = $email;
            $new_user->password = Hash::make($password);
            $new_user->save();
            return redirect()->route('users.index')->with('msg', 'Add User success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->check() == false) {
            return redirect('login');
        } else {
            if (auth()->user()->role != 'admin') {
                return abort(403, "No permission");
            }
        }

        $user = User::find($id);
        return view('editUSer', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        if (empty($name)) {
            return redirect()->back()->with('error', 'Invalid fields');
        }
        $data = $request->toArray();
        unset($data['_token']);
        unset($data['_method']);
        User::where('id', $id)
            ->update($data);
        return redirect()->route('users.index')->with('msg', 'Update user Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return \Redirect::back()->with('msg', 'Delete user Success');
    }
}
