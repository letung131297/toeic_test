<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{

    protected function subjectCode()
    {
        return $this->belongsTo('App\SubjectCode', 'subject_code_id', 'id');
    }

    protected function questions()
    {
        return $this->hasMany('App\Question', 'part_id', 'id');
    }
}
