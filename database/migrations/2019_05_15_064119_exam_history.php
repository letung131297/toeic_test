<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExamHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subject_code_id');
            $table->unsignedInteger('user_id');
            $table->integer('listening_correct');
            $table->integer('reading_correct');
            $table->integer('total_grade');
            $table->timestamps();
            $table->foreign('subject_code_id')
                ->references('id')->on('subject_codes')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
