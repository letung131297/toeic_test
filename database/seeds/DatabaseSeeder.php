<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        /*$rows_subject_codes = 1;
        $rows_parts = 7;
        $rows_question = 0;

        for ($row = 1; $row <= $rows_subject_codes; $row++) {
            $this->call(SubjectCodesTableSeeder::class);
        }

        $this->call(PartsTableSeeder::class);*/

        /*for ($row = 0; $row < $rows_parts; $row++) {
            $this->call(PartsTableSeeder::class);
        }*/

        /*for ($row = 0; $row < $rows_question; $row++) {
            $this->call(QuestionsTableSeeder::class);
        }*/

    }
}
