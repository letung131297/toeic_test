@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif

            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="panel panel-default">

            <div class="panel-heading">

                <h1>Import Questions</h1>

            </div>

            <div class="panel-body">


                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('importQuestionExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

                    @csrf



                    @if ($errors->any())

                        <div class="alert alert-danger">

                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif



                    @if (Session::has('success'))

                        <div class="alert alert-success">

                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

                            <p>{{ Session::get('success') }}</p>

                        </div>

                    @endif

                    <label for="subject_code">Subject Code</label><br>
                    <select name="subject_code" id="subject_code" class="form-control">
                        @foreach($list_subject_code as $subject_code)
                            <option value="{{ $subject_code['id'] }}">{{ $subject_code['title'] }}</option>
                        @endforeach
                    </select>


                    <div class="form-group">
                        <label for="import_file">File</label>
                        <input type="file" class="form-control-file" id="import_file" name="import_file">
                    </div>

                    <button class="btn btn-primary">Import File</button>

                </form>



            </div>

        </div>

    </div>
@endsection
