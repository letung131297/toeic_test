@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif

            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <h1>List questions</h1>
            <table class="table table-bordered dataTable" id="datatable_paginate">
                <thead>
                <tr>
                    <th scope="col">Subject</th>
                    <th scope="col">Question</th>
                    <th scope="col">Correct Answer</th>
                    <th scope="col">Part</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $text_correct_answer = ['A', 'B', 'C', 'D'];
                ?>
                @foreach($listQuestion as $question)
                    <tr>
                        <td>
                            <a href="{{ route("subjects.edit", ["id" => $question->part->subjectCode->id]) }}" target="_blank">
                                {{ $question->part->subjectCode->title }}
                            </a>
                        </td>
                        <td>
                            @if ($question->attachment)
                                <img src="{{ $question->attachment }}" style="max-width: 80px;" alt="">
                            @endif
                            {{ $question->the_question }}
                        </td>
                        <td>{{ $text_correct_answer[$question->correct_answer - 1] }}</td>
                        <td>Part {{ $question->part->part }}</td>
                        <td>
                            <a href="{{ route('questions.edit', ['questions' => $question->id]) }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <link rel="stylesheet" href="{{ asset('lib_admin/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('lib_admin/datatables/jquery.dataTables.min.js') }}" defer></script>
    <script src="{{ asset('lib_admin/datatables/dataTables.bootstrap4.min.js') }}" defer></script>
    <script>
        $(document).ready(function() {
            $('#datatable_paginate').DataTable();
        });
    </script>
@endsection
